<?php

namespace Drupal\pluggable_entity_view_builder;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Url;
use Drupal\image\Entity\ImageStyle;

/**
 * Common methods for all field-able content entities' view builders.
 *
 * @property \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
 */
trait ContentEntityViewBuilderTrait {

  /**
   * Get the value of a text field on an entity as a string.
   *
   * Suitable for plain text fields mostly, but can work with any fields which
   * return string values.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The field name to get the value of.
   *
   * @return string
   *   The value or an empty string if the field is empty.
   */
  protected function getTextFieldValue(ContentEntityInterface $entity, string $field_name): string {
    if ($entity->get($field_name)->isEmpty()) {
      return '';
    }
    return $entity->get($field_name)->getString();
  }

  /**
   * Get the value of a boolean field on an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The field name to get the value of.
   *
   * @return bool
   *   Boolean value.
   */
  protected function getBooleanFieldValue(ContentEntityInterface $entity, string $field_name): bool {
    return boolval($this->getTextFieldValue($entity, $field_name));
  }

  /**
   * Get the formatted date from an entity's date field.
   *
   * Note that this is compatible with a single date field, not with date_range.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The name of the single date field.
   * @param string $date_format
   *   The date format for the output. Must be a PHP date format string.
   *
   * @return string
   *   The formatted date string, or an empty string if the field has no value.
   */
  protected function getDateFieldValue(ContentEntityInterface $entity, string $field_name, string $date_format): string {
    if ($entity->get($field_name)->isEmpty()) {
      return '';
    }
    /** @var \Drupal\datetime\Plugin\Field\FieldType\DateTimeFieldItemList $date_field */
    $date_field = $entity->get($field_name);
    if (!isset($date_field->date)) {
      return '';
    }
    /** @var \Drupal\Core\Datetime\DrupalDateTime $date */
    $date = $date_field->date;
    return $date->format($date_format);
  }

  /**
   * Get the URL and title of a link field on an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The link field name.
   *
   * @return array|null
   *   Array containing 2 items: The URL to of the link, and the link title
   *   respectively.
   */
  protected function getLinkFieldValue(ContentEntityInterface $entity, string $field_name): ?array {
    if ($entity->get($field_name)->isEmpty()) {
      return NULL;
    }
    $value = $entity->get($field_name)->getValue();
    $url = Url::fromUri($value[0]['uri']);
    $title = $value[0]['title'];

    return [
      'url' => $url,
      'title' => $title,
    ];
  }

  /**
   * Get the (styled) URL of an image on a media field on an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The media field name.
   * @param string $image_style
   *   The image style to use. If none then original image URL is returned.
   *
   * @return string
   *   Empty string if no image available, or the URL to an image style
   *   derivative if image style ID is given, otherwise the path to the original
   *   image.
   */
  protected function getMediaImageUrl(ContentEntityInterface $entity, string $field_name, string $image_style = ''): string {
    if ($entity->get($field_name)->isEmpty()) {
      return '';
    }
    // Get the Media entity.
    /** @var \Drupal\media\MediaInterface $media */
    $media = $this->getReferencedEntityFromField($entity, $field_name);
    if (empty($media) || !$media->access('view')) {
      return '';
    }
    // Get the Image entity from Media.
    /** @var \Drupal\file\FileInterface $image */
    $image = $this->getReferencedEntityFromField($media, 'field_media_image');

    if (!$image->access('download')) {
      // No access to view the image.
      return '';
    }

    if (!empty($image_style)) {
      // Load the image style.
      /** @var \Drupal\image\ImageStyleInterface $image_style */
      $style = ImageStyle::load($image_style);
      return $style->buildUrl($image->getFileUri());
    }
    // Return the URL to the original file.
    return file_create_url($image->getFileUri());
  }

  /**
   * Get the alt of an image on a media field on an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The media field name.
   *
   * @return string
   *   The image alt.
   */
  protected function getMediaImageAlt(ContentEntityInterface $entity, string $field_name): string {
    if ($entity->get($field_name)->isEmpty()) {
      return '';
    }
    $field = $this->getImageFieldFromMediaEntity($entity, $field_name);
    if (empty($field)) {
      return '';
    }
    return $field->getValue()[0]['alt'] ?? '';
  }

  /**
   * Get the image field from a media entity on an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The name of the media field.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface|null
   *   The image field on the media entity.
   */
  protected function getImageFieldFromMediaEntity(ContentEntityInterface $entity, string $field_name): ?FieldItemListInterface {
    // Get the media entity.
    /** @var \Drupal\media\MediaInterface $media */
    $media = $this->getReferencedEntityFromField($entity, $field_name);
    if (empty($media) || $media->field_media_image->isEmpty() || empty($media->field_media_image->entity)) {
      return NULL;
    }
    return $media->field_media_image;
  }

  /**
   * Get a referenced entity from a field.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The reference field name.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The referenced entity or null if the field is empty or the entity isn't
   *   found.
   */
  protected function getReferencedEntityFromField(ContentEntityInterface $entity, string $field_name): ?EntityInterface {
    // Get the media entity.
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $field */
    $field = $entity->get($field_name);
    if ($field->isEmpty() || empty($field->entity)) {
      // Field is empty or referenced entity has been deleted.
      return NULL;
    }
    return $field->entity;
  }

  /**
   * Build an image referenced in the given entity's given field name.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   Optional; The field name. Defaults to "field_image".
   * @param string $image_style
   *   Optional; The image style. The original URL is returned if no style.
   *
   * @return array
   *   An array containing url and alt.
   */
  protected function buildImage(ContentEntityInterface $entity, string $field_name = 'field_image', string $image_style = ''): array {
    $image_info = $this->getImageAndAlt($entity, $field_name);
    if (!$image_info) {
      return [];
    }

    if (!empty($image_style)) {

      return [
        '#theme' => 'image_style',
        '#style_name' => $image_style,
        '#uri' => $image_info['uri'],
        '#alt' => $image_info['alt'],
      ];
    }

    return [
      '#theme' => 'image',
      '#uri' => $image_info['uri'],
      '#alt' => $image_info['alt'],
    ];

  }

  /**
   * Get image and alt from a File field.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   Optional; The field name. Defaults to "field_image".
   *
   * @return array|null
   *   An array containing uri and alt.
   */
  protected function getImageAndAlt(ContentEntityInterface $entity, string $field_name = 'field_image'): ?array {
    if (empty($entity->{$field_name}) || $entity->get($field_name)->isEmpty()) {
      // No field, or it's empty.
      return NULL;
    }

    /** @var \Drupal\file\FileInterface $image */
    $image = $entity->get($field_name)[0]->entity;
    if (empty($image) || !$image->access('download')) {
      return NULL;
    }

    return [
      'uri' => file_create_url($image->getFileUri()),
      'alt' => $entity->get($field_name)[0]->alt ?: '',
    ];
  }

  /**
   * Build a responsive image render array from an entity's media field.
   *
   * Note: Responsive Image core module is required for this to work.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The field name.
   * @param string $responsive_image_style_id
   *   The responsive image style ID.
   *
   * @return array
   *   A render array or empty array if no value in field or no access to view
   *   the image.
   */
  protected function buildMediaResponsiveImage(ContentEntityInterface $entity, string $field_name, string $responsive_image_style_id): array {
    if ($entity->get($field_name)->isEmpty()) {
      return [];
    }
    /** @var \Drupal\media\MediaInterface $media */
    $media = $this->getReferencedEntityFromField($entity, $field_name);
    if (empty($media)) {
      // No media to display.
      return [];
    }

    /** @var \Drupal\file\FileInterface $image */
    $image = $this->getReferencedEntityFromField($media, 'field_media_image');
    if (empty($image) || !$image->access('download')) {
      return [];
    }

    $cache = CacheableMetadata::createFromObject($media);
    $element = [
      '#theme' => 'responsive_image',
      '#uri' => $image->getFileUri(),
      '#responsive_image_style_id' => $responsive_image_style_id,
      '#attributes' => [
        'alt' => $this->getMediaImageAlt($entity, $field_name),
      ],
    ];

    $cache->addCacheableDependency($image);
    $cache->applyTo($element);

    return $element;
  }

  /**
   * Build entities in given view mode, from a reference field.
   *
   * @param \Drupal\Core\Field\EntityReferenceFieldItemListInterface $reference_field
   *   The field object where the referenced items are stored.
   * @param string $view_mode
   *   Optional; The view mode to build. Defaults to "full".
   *
   * @return array
   *   An array of render arrays.
   */
  protected function buildReferencedEntities(EntityReferenceFieldItemListInterface $reference_field = NULL, string $view_mode = 'full'): array {
    if (empty($reference_field) || $reference_field->isEmpty()) {
      // Field doesn't exist, or is empty.
      return [];
    }

    $element = [];
    $cache = CacheableMetadata::createFromRenderArray($element);

    $entity_type_id = $reference_field->getFieldDefinition()->getFieldStorageDefinition()->getSetting('target_type');
    $view_builder = $this->entityTypeManager->getViewBuilder($entity_type_id);

    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $field */
    foreach ($reference_field->referencedEntities() as $entity) {
      $element[] = $view_builder->view($entity, $view_mode);
      $cache->addCacheableDependency($entity);
    }

    $cache->applyTo($element);

    return $element;
  }

}
